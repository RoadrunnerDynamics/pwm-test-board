#include <Servo.h>

// Define pins
const int servo_pwm_pin = 13;
const int pot_pin = 14;
const int button_pin = 15;

// Create servo object
Servo myservo;

// Variables to store potentiometer and button states
int pot_val = 0;
int angle = 0; // Variable to store the servo position
bool last_button_state = HIGH; // Assume button is released initially
int last_written_angle = -1; // Store the last angle written to the servo

void setup() {
  Serial.begin(9600);
  myservo.attach(servo_pwm_pin); // Attach the servo to the PWM pin
  pinMode(pot_pin, INPUT);
  pinMode(button_pin, INPUT_PULLUP); // Set the button pin as input with pull-up
}

void loop() {
  pot_val = analogRead(pot_pin); // Read the potentiometer value
  bool button_state = digitalRead(button_pin); // Read the button state

  // Map potentiometer value (0-1024) to angle (0-180)
  angle = map(pot_val, 0, 1024, 0, 180);

  // Update servo if the button is pressed (button state goes from HIGH to LOW)
  if (last_button_state == HIGH && button_state == LOW) {
    myservo.write(angle); // Move the servo to the mapped angle
    last_written_angle = angle; // Update the last written angle
    Serial.println("Button pressed, servo position synced.");
  }
  last_button_state = button_state; // Update the last button state

  // Check for significant change in potentiometer value and update servo accordingly, if different from last written angle
  if (abs(angle - last_written_angle) > 1) {
    myservo.write(angle); // Move the servo to the mapped angle
    last_written_angle = angle; // Update the last written angle
    Serial.print("Pot Val: ");
    Serial.println(pot_val);
    Serial.print("Moving Servo to: ");
    Serial.println(angle);
  }

  delay(10); // Short delay to debounce button press and stabilize potentiometer reading
}
